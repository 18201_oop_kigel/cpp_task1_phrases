
#include "count_p.h"

using namespace std;


map <string, int> freq;

bool compare(string s1, string  s2)
{
	return freq[s2] > freq[s1];
}


int count_p(istream &enter, ostream &exit,int N, int M)
{
	vector <string> window ((unsigned)N);
	vector <string> check;

	string st;

	int count = 0;

	for(int i = 0; i < N; i++)
	{
		enter >> window[i];
		st +=  window[i] + " ";
		if(window[i] != "")
		{
			count++;
		}
	}
    if(count != N)
    {
        return 0;
    }

	if(++freq[st] == M)
	{
		check.push_back(st);
	}


	string new_word;

	while(enter >> new_word)
	{
		window.erase(window.begin());
		window.push_back(new_word);

		string str_p;

		for(int i = 0; i < N; i++)
		{
			 str_p+= window[i] + " ";
		}

		if(++freq[str_p] == M)
		{
			check.push_back(str_p);
		}
	}

	std::sort(check.begin(),check.end(), compare);

	while(!check.empty())
	{
		exit << check.back() << "(" << freq[check.back()] << ")" << endl;
		check.pop_back();
	}
	return 0;
}


void rasbor(int count, char *vvod[], int &N, int &M, string &text)
{
    M = 2;
    N = 2;

    text = "-";

    for(int i = 1; i < count; i++)
    {
        if(strcmp(vvod[i], "-n") == 0)
        {
            N = stoi(vvod[i + 1]);
            i++;
        }
        if(strcmp(vvod[i], "-m") == 0)
        {
            M = stoi(vvod[i + 1]);
            i++;
        }

        else
        {
            text = vvod[i];
        }
    }

}