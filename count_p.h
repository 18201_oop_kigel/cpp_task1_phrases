
#ifndef COUNT_P_H
#define COUNT_P_H

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <map>
#include <fstream>
#include <vector> //вместо динамического массива
#include <queue>

using namespace std;

int count_p(istream &enter, ostream &exit,int N, int M);
bool compare(string  s1, string  s2);
void rasbor(int count, char *vvod[], int &N, int &M, string &text);

#endif