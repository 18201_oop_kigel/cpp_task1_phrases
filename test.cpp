#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "count_p.h"
#include <sstream>



TEST_CASE("Test_1")
{
	std::stringstream in;
	std::stringstream out;

	in << "Alie parusa Asol" << endl;

	for(int i = 0; i < 2; i++)
	{
		in << "Alie parusa Plus Gray" << endl;
	}

	in << "Alie parusa parusa" << endl;

	count_p(in, out, 2, 3);

	REQUIRE(out.str() == "Alie parusa (4)\n");
}
TEST_CASE("test_2")
{
	std::stringstream in;
	std::stringstream out;

	for(int i = 0; i < 3; i++)
	{
		in << "Alie Plus" << endl;
	}
	for(int i = 0; i < 4; i++)
	{
		in << "Asol Gray" << endl;

	}


	count_p(in, out, 2, 3);

	REQUIRE(out.str() == "Asol Gray (4)\nGray Asol (3)\nAlie Plus (3)\n");

}
TEST_CASE("test_3")
{
	std::stringstream in;
	std::stringstream out;

	for(int i = 0; i < 3; i++)
	{
		in << "Alie parusa Plus Gray" << endl;
	}

	in << "Alie parusa Plus Asol" << endl;


	count_p(in, out, 3, 4);

	REQUIRE(out.str() == "Alie parusa Plus (4)\n");

}

TEST_CASE("Test_4", "Vse ploho") {

	stringstream in;
	stringstream out;

	in << "Alie parusa" << endl;

	count_p(in, out, 10, 4);

	REQUIRE (out.str() == "");
}

TEST_CASE("rasb")
{

	stringstream exit;
	char * vvod[]= {"path", "-n", "4", "-m", "3", "in.txt"};

	int N, M;
	string  text;
	rasbor(6,vvod, N, M, text);
	bool sravnenie = (text == "in.txt");

	cout << text;

    REQUIRE((N == 4));
    REQUIRE((M == 3));
    REQUIRE(sravnenie);

}